#!/usr/bin/env bash

# Use the codeship_gce_service to authenticate
echo "Authenticating"
codeship_google authenticate

# gcloud default values
gcloud config set compute/zone us-central1-a
gcloud config set project $GOOGLE_PROJECT_ID
# connect to the cluster running the app
gcloud container clusters get-credentials cluster-1

NAMESPACE="staging"
if [ ! -z "$1" ]
then
  NAMESPACE="$1"
fi

IMAGE_NAME=gcr.io/cat-api-209111/app

kubectl -n $NAMESPACE apply -f k8s/kubernetes.yml

TAG=$(echo $CI_COMMIT_ID | cut -c1-8)
if [ "$NAMESPACE" == "production" ]
then
    TAG=$CI_BRANCH
fi

kubectl -n $NAMESPACE set image deployment/cat-api cat-api=$IMAGE_NAME:$TAG
