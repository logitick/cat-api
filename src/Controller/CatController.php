<?php


namespace App\Controller;


use Faker\Factory;
use Symfony\Component\HttpFoundation\Response;

class CatController
{

    private $faker;

    public function __construct()
    {
        $this->faker = Factory::create();
    }

    function cat() {
        $image = file_get_contents($this->faker->imageUrl(1000, 1000, 'cats'));
        return new Response($image, 200, ['Content-type' => 'image/jpeg']);
    }
}