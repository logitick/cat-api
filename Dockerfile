FROM composer:1.6.5 as composer

WORKDIR /app
ADD composer.json ./
ADD composer.lock ./
RUN composer install --no-scripts --optimize-autoloader --no-dev

FROM php:7.1.19-apache

RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri "2i SetEnv APP_ENV ${APP_ENV}" /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

RUN mkdir /app
COPY . /app
COPY --from=composer /app/vendor /app/vendor
RUN chown -R www-data:www-data /app

RUN a2enmod rewrite
