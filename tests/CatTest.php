<?php


namespace App\Tests;



use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CatTest extends WebTestCase
{
    /**
     * @test
     */
    public function it_should_return_an_image() {
        $client = static::createClient();
        $client->request('GET', '/');

        $response = $client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals("image/jpeg", $response->headers->get("Content-type"));
    }
}